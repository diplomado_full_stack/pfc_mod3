CREATE TABLE public.usuario
(
    id integer NOT NULL DEFAULT nextval('usuario_id_seq'::regclass),
    cedula_identidad integer NOT NULL,
    nombre character varying(500),
    primer_apellido character varying(50),
    segundo_apellido character varying(50),
    fecha_nacimiento date,
    CONSTRAINT usuario_pkey PRIMARY KEY (id)
);

--insert
INSERT INTO public.usuario(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento)
	VALUES (1111,'juan','Terra','Men','2023-08-16');
INSERT INTO public.usuario(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento)
	VALUES (2222,'Manuel','vasquez','Men','2023-08-16');
INSERT INTO public.usuario(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento)
	VALUES (3333,'Jaima','Soud','Balderas','2023-08-16');
INSERT INTO public.usuario(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento)
	VALUES (4444,'Paola','Hess','indi','1986-08-01');
INSERT INTO public.usuario(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento)
	VALUES (5555,'Michel','Osinaga','Men','1990-01-5');