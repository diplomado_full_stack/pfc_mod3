from .entities.Mensaje import Mensaje


class MensajeModel():

    @classmethod
    def get_info(self):
        try:
            info = Mensaje("api-users","0.0.1","Alan Vasquez Balderas","alanvba@gmail.com")
            return info.to_JSON()
        except Exception as ex:
            raise Exception(ex)