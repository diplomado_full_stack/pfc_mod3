from database.db import get_connection
from .entities.Usuario import Usuario


class UsuarioModel():

    @classmethod
    def getAll(self):
        try:
            connection = get_connection()
            listUsuarios = []

            with connection.cursor() as cursor:
                cursor.execute("SELECT id, cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento FROM public.usuario ORDER BY primer_apellido ASC")
                resultset = cursor.fetchall()

                for row in resultset:
                    usuario = Usuario(row[0], row[1], row[2], row[3], row[4], row[5])
                    listUsuarios.append(usuario.to_JSON())

            connection.close()
            return listUsuarios
        except Exception as ex:
            raise Exception(ex)

    @classmethod
    def getById(self, id):
        try:
            connection = get_connection()

            with connection.cursor() as cursor:
                cursor.execute("SELECT id, cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento FROM public.usuario WHERE id = %s", (id,))
                row = cursor.fetchone()

                usuario = None
                if row != None:
                    usuario = Usuario(row[0], row[1], row[2], row[3], row[4], row[5])
                    usuario = usuario.to_JSON()

            connection.close()
            return usuario
        except Exception as ex:
            raise Exception(ex)

    @classmethod
    def addUsuario(self, usuario):
        try:
            connection = get_connection()

            with connection.cursor() as cursor:
                cursor.execute("""INSERT INTO public.usuario (cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) 
                                VALUES (%s, %s, %s, %s, %s)""", (usuario.cedula_identidad,usuario.nombre,usuario.primer_apellido,usuario.segundo_apellido,usuario.fecha_nacimiento))
                affected_rows = cursor.rowcount
                connection.commit()

            connection.close()
            return affected_rows
        except Exception as ex:
            raise Exception(ex)

    @classmethod
    def updateUsuario(self, usuario):
        try:
            connection = get_connection()

            with connection.cursor() as cursor:
                cursor.execute("""UPDATE public.usuario SET cedula_identidad = %s, nombre = %s, primer_apellido = %s,segundo_apellido = %s,fecha_nacimiento = %s
                                WHERE id = %s""", (usuario.cedula_identidad, usuario.nombre, usuario.primer_apellido, usuario.segundo_apellido,usuario.fecha_nacimiento,usuario.id))
                affected_rows = cursor.rowcount
                connection.commit()

            connection.close()
            return affected_rows
        except Exception as ex:
            raise Exception(ex)

    @classmethod
    def deleteUsuario(self, usuario):
        try:
            connection = get_connection()

            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM public.usuario WHERE id = %s", (usuario.id,))
                affected_rows = cursor.rowcount
                connection.commit()

            connection.close()
            return affected_rows
        except Exception as ex:
            raise Exception(ex)

    @classmethod
    def promedioEdad(self):
        try:
            connection = get_connection()
            with connection.cursor() as cursor:
                cursor.execute("SELECT AVG(EXTRACT(YEAR FROM AGE(NOW(),fecha_nacimiento))) AS promedio_edades FROM public.usuario")
                row = cursor.fetchone()
                
                promedio = None
                if row != None:
                    promedio = {"promedioEdad":row[0]}

            connection.close()
            return promedio
        except Exception as ex:
            raise Exception(ex)