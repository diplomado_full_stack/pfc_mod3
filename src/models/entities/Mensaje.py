class Mensaje():
    def __init__(self, nameSystem, version, developer, email) -> None:
        self.nameSystem = nameSystem
        self.version = version
        self.developer = developer
        self.email = email

    def to_JSON(self):
        return {
            'nameSystem': self.nameSystem,
            'version': self.version,
            'developer': self.developer,
            'email': self.email
        }

