from flask import Blueprint, jsonify, request

# Entities
from models.entities.Usuario import Usuario
# Models
from models.MensajeModel import MensajeModel
from models.UsuarioModel import UsuarioModel

main = Blueprint('usuario_blueprint', __name__)


@main.route('/')
def getEstado():
    try:
        entity = {"Estado":True,"message":"Sistema de seguimiento academico."}
        return jsonify(entity)
    except Exception as ex:
        return jsonify({'message': str(ex),"Estado":False}), 500

@main.route('/status')
def getInfo():
    try:
        info = MensajeModel.get_info()
        return jsonify(info)
    except Exception as ex:
        return jsonify({'message': str(ex)}), 500
    
@main.route('/usuarios/<id>')
def getUsuario(id):
    try:
        usuario = UsuarioModel.getById(id)
        if usuario != None:
            return jsonify(usuario)
        else:
            return jsonify({}), 404
    except Exception as ex:
        return jsonify({'message': str(ex)}), 500

@main.route('/usuarios')
def getUsuarios():
    try:
        usuario = UsuarioModel.getAll()
        if usuario != None:
            return jsonify(usuario)
        else:
            return jsonify([]), 404
    except Exception as ex:
        return jsonify({'message': str(ex)}), 500

@main.route('/save', methods=['POST'])
def add_usuario():
    try:
        cedula_identidad = int(request.json['cedula_identidad'])
        nombre = request.json['nombre']
        primer_apellido = request.json['primer_apellido']
        segundo_apellido = request.json['segundo_apellido']
        fecha_nacimiento = request.json['fecha_nacimiento']
        usuario = Usuario(None,cedula_identidad,nombre,primer_apellido,segundo_apellido,fecha_nacimiento)
  
        affected_rows = UsuarioModel.addUsuario(usuario)

        if affected_rows == 1:
            return jsonify(affected_rows)
        else:
            return jsonify({'message': "Error al momento de insertar"}), 500

    except Exception as ex:
        return jsonify({'message': str(ex)}), 500


@main.route('/update/<id>', methods=['PUT'])
def updateUsuario(id):
    try:
        cedula_identidad = int(request.json['cedula_identidad'])
        nombre = request.json['nombre']
        primer_apellido = request.json['primer_apellido']
        segundo_apellido = request.json['segundo_apellido']
        fecha_nacimiento = request.json['fecha_nacimiento']
        usuario = Usuario(id,cedula_identidad,nombre,primer_apellido,segundo_apellido,fecha_nacimiento)
        
        affected_rows = UsuarioModel.updateUsuario(usuario)

        if affected_rows == 1:
            return jsonify(usuario.id)
        else:
            return jsonify({'message': "Error al momento de actualizar."}), 404

    except Exception as ex:
        return jsonify({'message': str(ex)}), 500


@main.route('/delete/<id>', methods=['DELETE'])
def deleteUsuario(id):
    try:
        usuario = Usuario(id)

        affected_rows = UsuarioModel.deleteUsuario(usuario)

        if affected_rows == 1:
            return jsonify(usuario.id)
        else:
            return jsonify({'message': "Error al momento de eliminar."}), 404

    except Exception as ex:
        return jsonify({'message': str(ex)}), 500

@main.route('/promedio-edad')
def promedioEdad():
    try:
        promedio = UsuarioModel.promedioEdad()
        if promedio != None:
            return jsonify(promedio)
        else:
            return jsonify({}), 404
    except Exception as ex:
        return jsonify({'message': str(ex)}), 500