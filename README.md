Nombre del Diplomante:
### Alan Vasquez Balderas

Los scripts de la DB se incluye en la raiz del proyecto
### base_datos.sql

Primero, crear un entorno virtual:
### `python -m virtualenv env`

Para instalar los paquetes necesarios:
### `pip install -r requirements.txt`

Crear un archivo .env (en la raíz del proyecto) para las variables de entorno:

### `SECRET_KEY=SECRET_KEY`
### `PGSQL_HOST=host`
### `PGSQL_USER=user`
### `PGSQL_PASSWORD=password`
### `PGSQL_DB=database`

endpoint creados:
route:
### http://127.0.0.1:5000/api/v1

0.- GET endpoint estado webservice
### /
1.- GET endpoint para consultar informacion del webservice
### /status

2.-GET endpoint listar todos los usuarios
### /usuarios

3.- GET endpoint buscar un usuario por Id
### /usuarios/:id

4.- POST endpoint crear un usuario
### /save

5.- PUT endpoint modificar usuario
### /update/:id

6.- DELETE endpoint Eliminar usuario
## /delete/:id

7.- GET endpoint obtener promedio de edades
## /promedio-edad

Se incluye un archivo json con los testeo de los endpoint en postman
### testeo_endpoint_postman.json